package com.example.demo.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.Booking;

public interface BookingRepo extends JpaRepository<Booking, Integer>{

}
