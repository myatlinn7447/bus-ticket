package com.example.demo.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.Bus;

public interface BusRepo extends JpaRepository<Bus, Integer>{
	
	public List<Bus> findByLocation(String location);
}
