package com.example.demo.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.UserDetail;

public interface UserRepo extends JpaRepository<UserDetail, Integer>{
	
}
