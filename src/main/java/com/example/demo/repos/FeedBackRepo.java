package com.example.demo.repos;

import com.example.demo.entities.FeedBack;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedBackRepo extends JpaRepository<FeedBack,Integer>{

}