package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Booking;
import com.example.demo.entities.Bus;
import com.example.demo.repos.BookingRepo;
import com.example.demo.repos.BusRepo;

@RestController
@RequestMapping("booking")
@CrossOrigin
public class BookingContoller {

	@Autowired
	private BookingRepo bookingRepo;
	
	@Autowired
	private BusRepo busRepo;
	@GetMapping
	public List<Booking> findAll(){
		return bookingRepo.findAll();
	}
	
	@PostMapping("/create")
	public Booking createBooking(@RequestBody Booking booking) {
		Bus bus = busRepo.findById(booking.getBus().getId()).get();
		bus.setSeat(booking.getBus().getSeat() - booking.getSeats().size());
		System.out.println(booking);
		return this.bookingRepo.save(booking);
	}
	
	@DeleteMapping("/delete")
	public void deleteBooking(@RequestBody Booking booking) {
		this.bookingRepo.delete(booking);
	}
}
