package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Bus;
import com.example.demo.repos.BusRepo;

@RestController
@RequestMapping("buses")
@CrossOrigin
public class BusController {
	
	@Autowired
	private BusRepo busRepo;

	@GetMapping("/{location}")
	public List<Bus> findByIdBuses(@PathVariable String location){
		return busRepo.findByLocation(location);
	}
	
	@PostMapping("/create")
	public Bus createBus(@RequestBody Bus bus) {
		return busRepo.save(bus);
	}
}
