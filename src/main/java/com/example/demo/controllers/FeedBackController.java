package com.example.demo.controllers;

import java.util.List;
import com.example.demo.entities.FeedBack;
import com.example.demo.repos.FeedBackRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("feedbacks")
@CrossOrigin
@RestController
public class FeedBackController{

    @Autowired
    private FeedBackRepo feedBackRepo;

    @PostMapping("create")
    public FeedBack createFeedBack(@RequestBody FeedBack back){
    	System.out.println(back);
        return feedBackRepo.save(back);
    }

    @GetMapping
    public List<FeedBack> getAllFeedBacks(){
        return feedBackRepo.findAll();
    }
}