package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.UserDetail;
import com.example.demo.repos.UserRepo;

@RestController
@RequestMapping("users")
@CrossOrigin
public class UserController {
	
	@Autowired
	private UserRepo userRepo;
	
	@GetMapping
	public Iterable<UserDetail> all(){
		return userRepo.findAll();
	}
	
	@PostMapping("create")
	public UserDetail createUser(@RequestBody UserDetail user) {
		System.out.println(user);
		return userRepo.save(user);
	}
	
}
