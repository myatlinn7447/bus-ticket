insert into  bus (id,coach_type,fare,location,name,seat,time) values (1,'AC',10000,'YangontoMandalay','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (2,'AC',10000,'YangontoMandalay','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (3,'AC',10000,'YangontoMandalay','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (4,'AC',10000,'YangontoMandalay','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (5,'AC',10000,'YangontoMandalay','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (6,'AC',10000,'YangontoMandalay','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (7,'AC',10000,'YangontoMandalay','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (8,'AC',10000,'YangontoMandalay','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (9,'AC',12000,'YangontoKalaw','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (10,'AC',12000,'YangontoKalaw','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (11,'AC',12000,'YangontoKalaw','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (12,'AC',12000,'YangontoKalaw','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (13,'AC',12000,'YangontoKalaw','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (14,'AC',12000,'YangontoKalaw','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (15,'AC',12000,'YangontoKalaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (16,'AC',12000,'YangontoKalaw','JJ EXPRESS',36,'07:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (17,'AC',12000,'YangontoKyaiktiyo','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (18,'AC',12000,'YangontoKyaiktiyo','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (19,'AC',12000,'YangontoKyaiktiyo','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (20,'AC',12000,'YangontoKyaiktiyo','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (21,'AC',12000,'YangontoKyaiktiyo','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (22,'AC',12000,'YangontoBagan','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (23,'AC',12000,'YangontoBagan','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (24,'AC',12000,'YangontoBagan','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (25,'AC',12000,'YangontoBagan','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (26,'AC',12000,'YangontoBagan','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (27,'AC',12000,'YangontoChaungThar','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (28,'AC',12000,'YangontoChaungThar','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (29,'AC',12000,'YangontoChaungThar','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (30,'AC',12000,'YangontoChaungThar','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (31,'AC',12000,'YangontoChaungThar','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (32,'AC',12000,'YangontoInle','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (33,'AC',12000,'YangontoInle','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (34,'AC',12000,'YangontoInle','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (35,'AC',12000,'YangontoInle','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (36,'AC',12000,'YangontoInle','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (37,'AC',12000,'YangontoLashio','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (38,'AC',12000,'YangontoLashio','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (39,'AC',12000,'YangontoLashio','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (40,'AC',12000,'YangontoLashio','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (41,'AC',12000,'YangontoLashio','SHWE MANDALAR',36,'10:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (42,'AC',12000,'YangontoMonywa','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (43,'AC',12000,'YangontoMonywa','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (44,'AC',12000,'YangontoMonywa','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (45,'AC',12000,'YangontoMonywa','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (46,'AC',12000,'YangontoMonywa','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (47,'AC',12000,'YangontoMonywa','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (48,'AC',12000,'YangontoMonywa','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (49,'AC',12000,'YangontoMonywa','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (50,'AC',12000,'YangontoNaypyitaw','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (51,'AC',12000,'YangontoNaypyitaw','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (52,'AC',12000,'YangontoNaypyitaw','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (53,'AC',12000,'YangontoNaypyitaw','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (54,'AC',12000,'YangontoNaypyitaw','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (55,'AC',12000,'YangontoNaypyitaw','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (56,'AC',12000,'YangontoNaypyitaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (57,'AC',12000,'YangontoNaypyitaw','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (58,'AC',12000,'YangontoNgapali','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (59,'AC',12000,'YangontoNgapali','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (60,'AC',12000,'YangontoNgapali','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (61,'AC',12000,'YangontoNgapali','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (62,'AC',12000,'YangontoNgapali','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (63,'AC',12000,'YangontoNgapali','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (64,'AC',12000,'YangontoNgapali','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (65,'AC',12000,'YangontoNgapali','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (66,'AC',12000,'BagantoMandalay','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (67,'AC',12000,'BagantoMandalay','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (68,'AC',12000,'BagantoMandalay','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (69,'AC',12000,'BagantoMandalay','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (70,'AC',12000,'BagantoMandalay','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (71,'AC',12000,'BagantoMandalay','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (72,'AC',12000,'BagantoMandalay','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (73,'AC',12000,'BagantoMandalay','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (74,'AC',12000,'BagantoKalaw','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (75,'AC',12000,'BagantoKalaw','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (76,'AC',12000,'BagantoKalaw','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (77,'AC',12000,'BagantoKalaw','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (78,'AC',12000,'BagantoKalaw','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (79,'AC',12000,'BagantoKalaw','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (80,'AC',12000,'BagantoKalaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (81,'AC',12000,'BagantoKalaw','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (82,'AC',12000,'BagantoKyaiktiyo','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (83,'AC',12000,'BagantoKyaiktiyo','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (84,'AC',12000,'BagantoKyaiktiyo','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (85,'AC',12000,'BagantoKyaiktiyo','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (86,'AC',12000,'BagantoKyaiktiyo','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (87,'AC',12000,'BagantoKyaiktiyo','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (88,'AC',12000,'BagantoKyaiktiyo','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (89,'AC',12000,'BagantoKyaiktiyo','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (90,'AC',12000,'BagantoYangon','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (91,'AC',12000,'BagantoYangon','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (92,'AC',12000,'BagantoYangon','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (93,'AC',12000,'BagantoYangon','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (94,'AC',12000,'BagantoYangon','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (95,'AC',12000,'BagantoYangon','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (96,'AC',12000,'BagantoYangon','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (97,'AC',12000,'BagantoYangon','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (98,'AC',12000,'BagantoChaungThar','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (99,'AC',12000,'BagantoChaungThar','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (100,'AC',12000,'BagantoChaungThar','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (101,'AC',12000,'BagantoChaungThar','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (102,'AC',12000,'BagantoChaungThar','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (103,'AC',12000,'BagantoChaungThar','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (104,'AC',12000,'BagantoChaungThar','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (105,'AC',12000,'BagantoChaungThar','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (106,'AC',12000,'BagantoInle','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (107,'AC',12000,'BagantoInle','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (108,'AC',12000,'BagantoInle','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (109,'AC',12000,'BagantoInle','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (110,'AC',12000,'BagantoInle','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (111,'AC',12000,'BagantoLashio','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (112,'AC',12000,'BagantoLashio','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (113,'AC',12000,'BagantoLashio','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (114,'AC',12000,'BagantoLashio','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (115,'AC',12000,'BagantoLashio','SHWE MANDALAR',36,'10:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (116,'AC',12000,'BagantoMonywa','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (117,'AC',12000,'BagantoMonywa','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (118,'AC',12000,'BagantoMonywa','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (119,'AC',12000,'BagantoMonywa','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (120,'AC',12000,'BagantoMonywa','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (121,'AC',12000,'BagantoMonywa','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (122,'AC',12000,'BagantoMonywa','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (123,'AC',12000,'BagantoMonywa','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (124,'AC',12000,'BagantoNaypyitaw','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (125,'AC',12000,'BagantoNaypyitaw','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (126,'AC',12000,'BagantoNaypyitaw','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (127,'AC',12000,'BagantoNaypyitaw','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (128,'AC',12000,'BagantoNaypyitaw','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (129,'AC',12000,'BagantoNaypyitaw','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (130,'AC',12000,'BagantoNaypyitaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (131,'AC',12000,'BagantoNaypyitaw','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (132,'AC',12000,'BagantoNgapali','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (133,'AC',12000,'BagantoNgapali','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (134,'AC',12000,'BagantoNgapali','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (135,'AC',12000,'BagantoNgapali','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (136,'AC',12000,'BagantoNgapali','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (137,'AC',12000,'BagantoNgapali','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (138,'AC',12000,'BagantoNgapali','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (139,'AC',12000,'BagantoNgapali','JJ EXPRESS',36,'07:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (140,'AC',10000,'MandalaytoYangon','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (141,'AC',10000,'MandalaytoYangon','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (142,'AC',10000,'MandalaytoYangon','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (143,'AC',10000,'MandalaytoYangon','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (144,'AC',10000,'MandalaytoYangon','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (145,'AC',10000,'MandalaytoYangon','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (146,'AC',10000,'MandalaytoYangon','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (147,'AC',10000,'MandalaytoYangon','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (148,'AC',12000,'MandalaytoKalaw','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (149,'AC',12000,'MandalaytoKalaw','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (150,'AC',12000,'MandalaytoKalaw','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (151,'AC',12000,'MandalaytoKalaw','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (152,'AC',12000,'MandalaytoKalaw','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (153,'AC',12000,'MandalaytoKalaw','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (154,'AC',12000,'MandalaytoKalaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (155,'AC',12000,'MandalaytoKalaw','JJ EXPRESS',36,'07:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (156,'AC',12000,'MandalaytoKyaiktiyo','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (157,'AC',12000,'MandalaytoKyaiktiyo','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (158,'AC',12000,'MandalaytoKyaiktiyo','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (159,'AC',12000,'MandalaytoKyaiktiyo','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (160,'AC',12000,'MandalaytoKyaiktiyo','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (161,'AC',12000,'MandalaytoBagan','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (162,'AC',12000,'MandalaytoBagan','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (163,'AC',12000,'MandalaytoBagan','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (164,'AC',12000,'MandalaytoBagan','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (165,'AC',12000,'MandalaytoBagan','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (166,'AC',12000,'MandalaytoChaungThar','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (167,'AC',12000,'MandalaytoChaungThar','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (168,'AC',12000,'MandalaytoChaungThar','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (169,'AC',12000,'MandalaytoChaungThar','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (170,'AC',12000,'MandalaytoChaungThar','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (171,'AC',12000,'MandalaytoInle','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (172,'AC',12000,'MandalaytoInle','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (173,'AC',12000,'MandalaytoInle','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (174,'AC',12000,'MandalaytoInle','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (175,'AC',12000,'MandalaytoInle','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (176,'AC',12000,'MandalaytoLashio','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (177,'AC',12000,'MandalaytoLashio','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (178,'AC',12000,'MandalaytoLashio','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (179,'AC',12000,'MandalaytoLashio','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (180,'AC',12000,'MandalaytoLashio','SHWE MANDALAR',36,'10:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (181,'AC',12000,'MandalaytoMonywa','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (182,'AC',12000,'MandalaytoMonywa','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (183,'AC',12000,'MandalaytoMonywa','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (184,'AC',12000,'MandalaytoMonywa','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (185,'AC',12000,'MandalaytoMonywa','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (186,'AC',12000,'MandalaytoMonywa','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (187,'AC',12000,'MandalaytoMonywa','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (188,'AC',12000,'MandalaytoMonywa','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (189,'AC',12000,'MandalaytoNaypyitaw','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (190,'AC',12000,'MandalaytoNaypyitaw','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (191,'AC',12000,'MandalaytoNaypyitaw','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (192,'AC',12000,'MandalaytoNaypyitaw','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (193,'AC',12000,'MandalaytoNaypyitaw','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (194,'AC',12000,'MandalaytoNaypyitaw','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (195,'AC',12000,'MandalaytoNaypyitaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (196,'AC',12000,'MandalaytoNaypyitaw','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (197,'AC',12000,'MandalaytoNgapali','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (198,'AC',12000,'MandalaytoNgapali','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (199,'AC',12000,'MandalaytoNgapali','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (200,'AC',12000,'MandalaytoNgapali','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (201,'AC',12000,'MandalaytoNgapali','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (202,'AC',12000,'MandalaytoNgapali','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (203,'AC',12000,'MandalaytoNgapali','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (204,'AC',12000,'MandalaytoNgapali','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (205,'AC',10000,'KalawtoMandalay','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (206,'AC',10000,'KalawtoMandalay','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (207,'AC',10000,'KalawtoMandalay','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (208,'AC',10000,'KalawtoMandalay','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (209,'AC',10000,'KalawtoMandalay','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (210,'AC',10000,'KalawtoMandalay','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (211,'AC',10000,'KalawtoMandalay','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (212,'AC',10000,'KalawtoMandalay','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (213,'AC',12000,'KalawtoYangon','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (214,'AC',12000,'KalawtoYangon','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (215,'AC',12000,'KalawtoYangon','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (216,'AC',12000,'KalawtoYangon','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (217,'AC',12000,'KalawtoYangon','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (218,'AC',12000,'KalawtoYangon','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (219,'AC',12000,'KalawtoYangon','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (220,'AC',12000,'KalawtoYangon','JJ EXPRESS',36,'07:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (221,'AC',12000,'KalawtoKyaiktiyo','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (222,'AC',12000,'KalawtoKyaiktiyo','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (223,'AC',12000,'KalawtoKyaiktiyo','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (224,'AC',12000,'KalawtoKyaiktiyo','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (225,'AC',12000,'KalawtoKyaiktiyo','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (226,'AC',12000,'KalawtoBagan','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (227,'AC',12000,'KalawtoBagan','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (228,'AC',12000,'KalawtoBagan','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (229,'AC',12000,'KalawtoBagan','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (230,'AC',12000,'KalawtoBagan','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (231,'AC',12000,'KalawtoChaungThar','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (232,'AC',12000,'KalawtoChaungThar','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (233,'AC',12000,'KalawtoChaungThar','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (234,'AC',12000,'KalawtoChaungThar','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (235,'AC',12000,'KalawtoChaungThar','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (236,'AC',12000,'KalawtoInle','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (237,'AC',12000,'KalawtoInle','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (238,'AC',12000,'KalawtoInle','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (239,'AC',12000,'KalawtoInle','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (240,'AC',12000,'KalawtoInle','SHWE MANDALAR',36,'10:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (241,'AC',12000,'KalawtoLashio','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (242,'AC',12000,'KalawtoLashio','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (243,'AC',12000,'KalawtoLashio','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (244,'AC',12000,'KalawtoLashio','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (245,'AC',12000,'KalawtoLashio','SHWE MANDALAR',36,'10:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (246,'AC',12000,'KalawtoMonywa','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (247,'AC',12000,'KalawtoMonywa','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (248,'AC',12000,'KalawtoMonywa','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (249,'AC',12000,'KalawtoMonywa','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (250,'AC',12000,'KalawtoMonywa','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (251,'AC',12000,'KalawtoMonywa','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (252,'AC',12000,'KalawtoMonywa','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (253,'AC',12000,'KalawtoMonywa','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (254,'AC',12000,'KalawtoNaypyitaw','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (255,'AC',12000,'KalawtoNaypyitaw','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (256,'AC',12000,'KalawtoNaypyitaw','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (257,'AC',12000,'KalawtoNaypyitaw','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (258,'AC',12000,'KalawtoNaypyitaw','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (259,'AC',12000,'KalawtoNaypyitaw','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (260,'AC',12000,'KalawtoNaypyitaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (261,'AC',12000,'KalawtoNaypyitaw','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (262,'AC',12000,'KalawtoNgapali','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (263,'AC',12000,'KalawtoNgapali','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (264,'AC',12000,'KalawtoNgapali','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (265,'AC',12000,'KalawtoNgapali','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (266,'AC',12000,'KalawtoNgapali','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (267,'AC',12000,'KalawtoNgapali','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (268,'AC',12000,'KalawtoNgapali','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (269,'AC',12000,'KalawtoNgapali','JJ EXPRESS',36,'07:00 PM');



insert into  bus (id,coach_type,fare,location,name,seat,time) values (270,'AC',12000,'KyaiktiyotoYangon','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (271,'AC',12000,'KyaiktiyotoInle','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (272,'AC',12000,'KyaiktiyotoBagan','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (273,'AC',12000,'KyaiktiyotoMandalay','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (274,'AC',12000,'KyaiktiyotoChaungTha','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (275,'AC',12000,'KyaiktiyotoLashio','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (276,'AC',12000,'KyaiktiyotoNgapali','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (277,'AC',12000,'KyaiktiyotoNaypyitaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (278,'AC',12000,'KyaiktiyotoMonywa','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (279,'AC',12000,'InletoYangon','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (280,'AC',12000,'InletoBagan','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (281,'AC',12000,'InletoKyaiktiyo','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (282,'AC',12000,'InletoMandalay','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (283,'AC',12000,'InletoChaungTha','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (284,'AC',12000,'InletoLashio','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (285,'AC',12000,'InletoNgapali','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (286,'AC',12000,'InletoNaypyitaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (287,'AC',12000,'InletoMonywa','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (288,'AC',12000,'ChaungThatoYangon','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (289,'AC',12000,'ChaungThatoInle','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (290,'AC',12000,'ChaungThatoBagan','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (291,'AC',12000,'ChaungThatoMandalay','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (292,'AC',12000,'ChaungThatoKyaiktiyo','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (293,'AC',12000,'ChaungThatoLashio','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (294,'AC',12000,'ChaungThatoNgapali','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (295,'AC',12000,'ChaungThatoNaypyitaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (296,'AC',12000,'ChaungThatoMonywa','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (297,'AC',12000,'LashiotoYangon','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (298,'AC',12000,'LashiotoInle','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (299,'AC',12000,'LashiotoBagan','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (300,'AC',12000,'LashiotoMandalay','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (301,'AC',12000,'LashiotoChaungTha','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (302,'AC',12000,'LashiotoKyaiktiyo','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (303,'AC',12000,'LashiotoNgapali','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (304,'AC',12000,'LashiotoNaypyitaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (305,'AC',12000,'LashiotoMonywa','JJ EXPRESS',36,'07:00 PM');

insert into  bus (id,coach_type,fare,location,name,seat,time) values (306,'AC',12000,'NgapalitoYangon','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (307,'AC',12000,'NgapalitoInle','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (308,'AC',12000,'NgapalitoBagan','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (309,'AC',12000,'NgapalitoMandalay','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (310,'AC',12000,'NgapalitoChaungTha','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (311,'AC',12000,'NgapalitoLashio','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (312,'AC',12000,'NgapalitoKyaiktiyo','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (313,'AC',12000,'NgapalitoNaypyitaw','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (314,'AC',12000,'NgapalitoMonywa','JJ EXPRESS',36,'07:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (315,'AC',12000,'NaypyitawtoYangon','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (316,'AC',12000,'NaypyitawtoInle','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (317,'AC',12000,'NaypyitawtoBagan','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (318,'AC',12000,'NaypyitawtoMandalay','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (319,'AC',12000,'NaypyitawtoChaungTha','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (320,'AC',12000,'NaypyitawtoLashio','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (321,'AC',12000,'NaypyitawtoKyaiktiyo','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (322,'AC',12000,'NaypyitawtoNgapali','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (323,'AC',12000,'NaypyitawtoMonywa','JJ EXPRESS',36,'07:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (324,'AC',12000,'NaypyitawtoYangon','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (325,'AC',12000,'NaypyitawtoYangon','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (326,'AC',12000,'NaypyitawtoYangon','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (327,'AC',12000,'NaypyitawtoYangon','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (328,'AC',12000,'NaypyitawtoYangon','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (329,'AC',12000,'NaypyitawtoYangon','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (330,'AC',12000,'NaypyitawtoYangon','JJ EXPRESS',36,'07:00 PM');



insert into  bus (id,coach_type,fare,location,name,seat,time) values (331,'AC',12000,'NaypyitawtoInle','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (332,'AC',12000,'NaypyitawtoInle','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (333,'AC',12000,'NaypyitawtoInle','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (334,'AC',12000,'NaypyitawtoInle','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (335,'AC',12000,'NaypyitawtoInle','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (336,'AC',12000,'NaypyitawtoInle','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (337,'AC',12000,'NaypyitawtoInle','JJ EXPRESS',36,'07:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (338,'AC',12000,'NaypyitawtoBagan','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (339,'AC',12000,'NaypyitawtoBagan','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (340,'AC',12000,'NaypyitawtoBagan','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (341,'AC',12000,'NaypyitawtoBagan','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (342,'AC',12000,'NaypyitawtoBagan','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (343,'AC',12000,'NaypyitawtoBagan','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (344,'AC',12000,'NaypyitawtoBagan','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (345,'AC',12000,'NaypyitawtoBagan','JJ EXPRESS',36,'07:00 PM');


insert into  bus (id,coach_type,fare,location,name,seat,time) values (346,'AC',12000,'NaypyitawtoMandalay','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (347,'AC',12000,'NaypyitawtoMandalay','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (348,'AC',12000,'NaypyitawtoMandalay','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (349,'AC',12000,'NaypyitawtoMandalay','SHWE SIN SETKYAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (350,'AC',12000,'NaypyitawtoMandalay','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (351,'AC',12000,'NaypyitawtoMandalay','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (352,'AC',12000,'NaypyitawtoMandalay','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (353,'AC',12000,'NaypyitawtoMandalay','JJ EXPRESS',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (354,'AC',12000,'NaypyitawtoChaungTha','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (355,'AC',12000,'NaypyitawtoChaungTha','ELITE',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (356,'AC',12000,'NaypyitawtoChaungTha','SHWE MAN THU',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (357,'AC',12000,'NaypyitawtoChaungTha','MA HAR',36,'06:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (358,'AC',12000,'NaypyitawtoChaungTha','SHWE MANDALAR',36,'10:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (359,'AC',12000,'NaypyitawtoChaungTha','LUMBINI',36,'08:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (360,'AC',12000,'NaypyitawtoChaungTha','SHWE LOON PYAN',36,'07:00 PM');
insert into  bus (id,coach_type,fare,location,name,seat,time) values (361,'AC',12000,'NaypyitawtoChaungTha','JJ EXPRESS',36,'07:00 PM');
insert into user_detail(id,user_email,mobile,user_name,password) values (1,'user@d.com','09212121','user','user');
insert into booking(id,key,ticket_id,bus_id,user_id) values (1,'2019-08-05','ff232dd4-b381-4da8-9d1e-b75419aac644',1,1);
alter sequence bus_id_seq restart with 362;
alter sequence user_detail_id_seq restart with 2;
alter sequence booking_id_seq restart with 2;