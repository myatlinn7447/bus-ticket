import { Component, OnInit } from '@angular/core';
import { SelectBusService } from '../services/selectBus.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Journey_Route } from '../models/route.model';

@Component({
  selector: 'select-bus',
  templateUrl: './select-bus.component.html',
  styleUrls: ['./select-bus.component.css']
})
export class SelectBusComponent implements OnInit {

  pnumber = 1;
  today = new Date().toISOString().substring(0, 10);
  place: Place[] = [];


  constructor(
    private BusService: SelectBusService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.place[0] = new Place()
  }

  ngOnInit() {
    this.today = new Date().toISOString().substring(0, 10);
    console.log(this.today);
  }



  SearchBus(form: NgForm) {
    let leaving_form = form.value.leaving_form;
    let destination;
    this.place.filter(iteam => {
      if (iteam.key == form.value.going_to) {
        destination = iteam.value
      }
    })

    let date = form.value.depart_date;
    let route: Journey_Route = {
      leaving_form: leaving_form,
      going_to: destination,
      date: date
    }
    localStorage.setItem("route", JSON.stringify(route))
    let routeId = leaving_form + 'to' + destination;
    this.BusService.getRoueId(routeId);
    this.router.navigate(['user', 'search']);
  }

  leave(e) {

    let leavingfrom = e.target.value;
    console.log(leavingfrom)
    if (leavingfrom == 'Naypyitaw') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Lashio' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Yangon' },
        { key: '10', value: 'Ngapali' }

      ]
    }
    else if (leavingfrom == 'Mandalay') {
      this.place = [
        { key: '1', value: 'Yangon' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Lashio' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Ngapali' }



      ]
    }
    else if (leavingfrom == 'Bagan') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Lashio' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Ngapali' }


      ]
    } else if (leavingfrom == 'Yangon') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Lashio' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Ngapali' }


      ]
    } else if (leavingfrom == 'Kalaw') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Yangon' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Lashio' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Ngapali' }

      ]

    } else if (leavingfrom == 'Kyaiktiyo') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Yangon' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Lashio' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Ngapali' }

      ]
    } else if (leavingfrom == 'Bagan') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Yangon' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Lashio' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Ngapali' }

      ]

    } else if (leavingfrom == 'ChaungThar') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'Yangon' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Lashio' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Ngapali' }

      ]

    } else if (leavingfrom == 'Inle') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Yangon' },
        { key: '7', value: 'Lashio' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Ngapali' }

      ]
    } else if (leavingfrom == 'Lashio') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Yangon' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Ngapali' }

      ]
    } else if (leavingfrom == 'Monywa') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Yangon' },
        { key: '8', value: 'Lashio' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Ngapali' }

      ]
    } else if (leavingfrom == 'Ngapali') {
      this.place = [
        { key: '1', value: 'Mandalay' },
        { key: '2', value: 'Kalaw' },
        { key: '3', value: 'Kyaiktiyo' },
        { key: '4', value: 'Bagan' },
        { key: '5', value: 'ChaungThar' },
        { key: '6', value: 'Inle' },
        { key: '7', value: 'Yangon' },
        { key: '8', value: 'Monywa' },
        { key: '9', value: 'Naypyitaw' },
        { key: '10', value: 'Lashio' }

      ];
    }
  }
}
export class Place {
  key: string;
  value: string;
}
