export class Bus {
    id:string
    coachType:string;
    fare:number;
    location:string;
    name:string;
    seat:number;
    time:string;
}
