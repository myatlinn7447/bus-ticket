import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class UserService {
  private back = 'http://' + window.location.hostname + ':8080/users/';
  constructor(
    private http: HttpClient
  ) { }

  createUser(user) {
    console.log(user);
    return this.http.post(this.back + 'create', user);
  }

  findAllUsers() {
    return this.http.get(this.back);
  }
}
