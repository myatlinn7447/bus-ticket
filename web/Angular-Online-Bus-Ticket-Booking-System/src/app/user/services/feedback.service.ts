import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class FeedbackService {

  private root_url = 'http://' + window.location.hostname + ':8080/feedbacks';

  constructor(private http: HttpClient) { }

  public createFeedBack(feedback) {
    console.log(feedback);
    return this.http.post(this.root_url + '/create', feedback);
  }

  public getAllFeedBacks() {
    return this.http.get(this.root_url);
  }
}
