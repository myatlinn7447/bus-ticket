import { Injectable } from "@angular/core";
import { Journey } from "../models/journey.model";
import { HttpClient } from "@angular/common/http";
import { UserService } from "./user.service";
import { User } from "../models/user.model";
import { Router } from "@angular/router";
import { BehaviorSubject } from "rxjs";
import * as uuid from "uuid";
@Injectable()

export class BookingService {

    journey_info = new BehaviorSubject('')
    cast = this.journey_info.asObservable();
    private USerId;
    private Root_Url = "https://bdbusticket.firebaseio.com/";
    private back = 'http://' + window.location.hostname + ':8080/booking/';
    constructor(
        private http: HttpClient,
        private UserService: UserService,
        private router: Router
    ) { }

    // userBooking(jourey:Journey) {
    //     this.http.post(this.Root_Url+'user_booking',jourey)
    // }

    public findALlBookings() {
        return this.http.get(this.back);
    }

    async seatBooking(journey: Journey) {
        let bus = journey.bus;
        let booking = new Object();
        let key = journey.journey_route.date;
        let res = JSON.parse(localStorage.getItem('user'));
        booking = {
            key: key,
            ticketId: uuid.v4(),
            user: res,
            bus: bus,
            seats: journey.seats
        };
        this.checkCreatedBookingOrNot(booking);
    }

    public createUserID(user) {
        user = {
            userEmail: user.user_email,
            mobile: user.mobile,
            userName: user.user_name,
            password: user.password
        };
        return this.UserService.createUser(user);
    }

    private checkCreatedBookingOrNot(booking) {
        console.log(booking);
        this.http.post(this.back + 'create', booking).subscribe((res: any) => {
            this.createPrintView(res.ticketId)
        });
    }

    createPrintView(tketID) {
        let journey = JSON.parse(localStorage.getItem("journey"));
        let user = JSON.parse(localStorage.getItem("user"));
        let Ticket = {
            ticketId: tketID,
            journey: journey,
            user: user
        }
        this.getJourneyInfo(Ticket);
        this.router.navigate(['user', 'print']);
    }

    getJourneyInfo(Ticket) {
        this.journey_info.next(Ticket);
        localStorage.removeItem("journey");
        localStorage.removeItem("route");
        //        localStorage.removeItem("user");
    }

    cancelBooking(booking) {
      return this.http.request('delete' , this.back + 'delete', {body:  booking});
    }
    // async seatBooking(journey: Journey, user) {
    //     let busID = journey.bus.$key;
    //     let booking = new Object();
    //     let key = new Date(journey.journey_route.date).getTime();
    //     await this.createUserID(user).subscribe(
    //         res => {
    //             booking = {
    //                 user_id: res.id,
    //                 seats: journey.seats
    //             }
    //             this.chekBookingDate_BusInfo(key, journey, booking, busID);
    //         });
    // }

    private async chekBookingDate_BusInfo(key, journey, booking, busID) {
        let keys = [];
        this.http.get(this.Root_Url + 'booking.json')
            .subscribe(
                res => {
                    for (let key in res) {
                        keys.push(key)
                    }
                    if (keys.indexOf(String(key)) > -1) {
                        this.CheckBusID(busID, key, booking, journey);
                    }
                    else {
                        this.createBookingDate(journey, key, booking, busID);
                    }
                }
            );
    }

    private async createBookingDate(journey: Journey, key, booking, busID) {

        await this.create(journey, key, busID, booking)
        // await this.createBooking(booking, key,busID)

    }

    private async  create(journey: Journey, key, busID, booking) {
        let location = journey.journey_route.leaving_form + ' to ' + journey.journey_route.going_to;
        this.http.put(this.Root_Url + 'booking/' + key + '/' + busID + '.json', {

            bus: {
                location: location,
                name: journey.bus.name,
                coach_type: journey.bus.coachType,
                nfareame: journey.bus.fare,
                time: journey.bus.time,
                seat: journey.bus.seat
            }
        })
            .subscribe(res => {
                this.createBooking(booking, key, busID);
            },
                error => console.log(error))
    }

    private CheckBusID(busID, key, booking, journey) {
        let busidArray = [];
        this.http.get(this.Root_Url + 'booking/' + key + '.json')
            .subscribe(res => {
                for (let key in res) {
                    busidArray.push(key)
                }
                if (busidArray.indexOf(String(busID)) > -1) {
                    this.createBooking(booking, key, busID);
                }
                else {
                    this.create(journey, key, busID, booking);
                }
            });
    }


    private createBooking(booking, key, busID) {
        let tketID;
        this.http.post(this.Root_Url + 'booking/' + key + '/' + busID + '/seat_booking.json', booking)
            .subscribe(res => {
                for (let key in res) {
                    tketID = res[key]
                }
                this.createPrintView(tketID);
            },
                err => console.log(err));
    }

}
