import { FeedbackService } from './../services/feedback.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, DoCheck } from '@angular/core';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, DoCheck {

  constructor(private route: ActivatedRoute, private router: Router, private fService: FeedbackService) { }

  loggedIn: boolean;
  loggedInAsAdmin: boolean;
  ngOnInit() {

  }

  ngDoCheck() {
    console.log(localStorage.getItem('user') );
    if (localStorage.getItem('user') !== null) {
      this.loggedIn = true;
    }
    if( localStorage.getItem('user') === 'admin') {
      this.loggedInAsAdmin = true;
    }
  }

  onLogOut() {
    localStorage.removeItem('user');
    this.loggedIn = false;
    this.loggedInAsAdmin = false;
    this.router.navigateByUrl('/');
    }

  onFeedBack(message: string) {
    console.log(message);
    const feedback = {
      feedBackMessage : message,
      userDetail : JSON.parse(localStorage.getItem('user'))
    };
    this.fService.createFeedBack(feedback).subscribe(a => {
      console.log(a);
    });
  }
}
