import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  loggedInAsAdmin: boolean;

  constructor() { }

  ngOnInit() {
  }

  ngDoCheck() {
    this.loggedInAsAdmin = localStorage.getItem('user') === 'admin' ? false : true;
  }
}
