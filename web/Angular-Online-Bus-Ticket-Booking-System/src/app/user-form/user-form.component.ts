import { Component, OnInit } from '@angular/core';
import { Journey } from '../user/models/journey.model';
import { Router } from '@angular/router';
import { BookingService } from '../user/services/booking.service';
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  journey: Journey;

  signUpOrLogIn: boolean = false;

  constructor(
    private route: Router,
    private bookingService: BookingService
  ) { }

  ngOnInit() {
    this.journey = JSON.parse(localStorage.getItem("journey"))
    if (!this.journey) {
      this.route.navigate([''])
    }
  }

  ngDoCheck(){
    if (localStorage.getItem('user') !== null) {
      this.signUpOrLogIn = true;
    }
  }

  onSubmit(){
    this.bookingService.seatBooking(this.journey);
  }
}
