import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BookingService } from '../../user/services/booking.service';
import { Journey } from '../../user/models/journey.model';
import { PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {


  constructor(private BookingService: BookingService, private location: PlatformLocation) { }

  ngOnInit() {
  }

  userForm(form: NgForm) {
    let name = form.value.user_name;
    let mobile_no = form.value.user_mobile;
    let email = form.value.user_email;
    let password = form.value.password;
    let user = {
      user_email: email,
      mobile: mobile_no,
      user_name: name,
      password: password
    }
    this.BookingService.createUserID(user).subscribe(a => {
      localStorage.setItem('user', JSON.stringify(a));
    });
    this.location.back();
  }
}
