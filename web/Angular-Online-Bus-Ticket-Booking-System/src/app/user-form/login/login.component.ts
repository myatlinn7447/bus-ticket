import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PlatformLocation } from '@angular/common';
import { UserService } from '../../user/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private location: PlatformLocation, private userService: UserService,private route: Router) { }

  ngOnInit() {
  }

  onLogIn(form: NgForm) {
    console.log(form.value);
    let value = form.value;
    if (form.value.user === 'admin' && form.value.password === 'admin') {
      localStorage.setItem('user', 'admin');
      this.route.navigateByUrl('/admin');
    } else {
      this.userService.findAllUsers().subscribe((a: any[]) => {
        console.log(value);
        a.forEach(i => {
          if (i.userName === value.user && i.password === value.password) {
            console.log(i)
            localStorage.setItem('user', JSON.stringify(i));
          }
        });
      });
      this.location.back();
    }
  }
}
