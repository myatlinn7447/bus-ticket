import { FeedbackService } from './user/services/feedback.service';
import { ViewUsersComponent } from './admin/view-users/view-users.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { HeaderComponent } from './user/header/header.component';
import { FooterComponent } from './user/footer/footer.component';
import { SelectBusComponent } from './user/select-bus/select-bus.component';
import { SelectSeatComponent } from './user/select-seat/select-seat.component';
import { BusSearchResultComponent } from './user/bus-search-result/bus-search-result.component';
import { SelectBusService } from './user/services/selectBus.service';
import { UserFormComponent } from './user-form/user-form.component';
import { BookingService } from './user/services/booking.service';
import { UserService } from './user/services/user.service';
import { PrintComponent } from './user/print/print.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './user-form/login/login.component';
import { AuthService } from './auth.service';
import { SignUpComponent } from './user-form/sign-up/sign-up.component';
import { EditBusComponent } from './admin/edit-bus/edit-bus.component';
import { ViewBookingsComponent } from './admin/view-bookings/view-bookings.component';
import { ViewFeedbackComponent } from './admin/view-feedback/view-feedback.component';

const userRoute: Routes = [

  { path: '', redirectTo: 'user', pathMatch: 'full' },
  {
    path: 'user', children: [
      { path: '', component: SelectBusComponent },
      { path: 'search', component: BusSearchResultComponent },
      { path: 'print', component: PrintComponent },
    ]
  },
  {
    path: 'details', children: [
      { path: '', component: UserFormComponent },
      { path: 'login', component: LoginComponent },
      { path: 'signUp', component: SignUpComponent }
    ]
  },
  {
    path: 'admin', children: [
      { path: '', component: AdminComponent },
      { path: 'edit-bus', component: EditBusComponent },
      { path: 'view-bookings', component: ViewBookingsComponent },
      { path: 'view-users', component: ViewUsersComponent },
      { path: 'view-feedbacks', component: ViewFeedbackComponent }
    ], canActivate: [AuthService]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HeaderComponent,
    FooterComponent,
    SelectBusComponent,
    SelectSeatComponent,
    BusSearchResultComponent,
    UserFormComponent,
    PrintComponent,
    AdminComponent,
    LoginComponent,
    SignUpComponent,
    EditBusComponent,
    ViewBookingsComponent,
    ViewUsersComponent,
    ViewFeedbackComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(userRoute),
    HttpClientModule,
    ModalModule.forRoot(),
    FormsModule
  ],
  providers: [
    SelectBusService,
    BookingService,
    UserService,
    AuthService,
    FeedbackService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
