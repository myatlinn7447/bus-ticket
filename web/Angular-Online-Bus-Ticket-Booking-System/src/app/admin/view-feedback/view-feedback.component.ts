import { FeedbackService } from './../../user/services/feedback.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-feedback',
  templateUrl: './view-feedback.component.html',
  styleUrls: ['./view-feedback.component.css']
})
export class ViewFeedbackComponent implements OnInit {

  feedbacks: any[];

  constructor(private fbService: FeedbackService) { }

  ngOnInit() {
    this.fbService.getAllFeedBacks().subscribe((a:any[]) => {
      console.log(a);
      this.feedbacks = a;});
  }

}
