import { Bus } from './../../user/models/bus.model';
import { Component, OnInit } from '@angular/core';
import { SelectBusService } from '../../user/services/selectBus.service';

@Component({
  selector: 'app-edit-bus',
  templateUrl: './edit-bus.component.html',
  styleUrls: ['./edit-bus.component.css']
})
export class EditBusComponent implements OnInit {

  bus: Bus = new Bus();

  constructor(private busService: SelectBusService) { }

  ngOnInit() {
  }

  onSubmit(){
    this.busService.createBus(this.bus).subscribe(
      a => {
        console.log(a);
      }
    );
  }
}
