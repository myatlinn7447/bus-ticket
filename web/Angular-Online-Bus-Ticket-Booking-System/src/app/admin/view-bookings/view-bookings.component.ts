import { Component, OnInit, DoCheck } from '@angular/core';
import { BookingService} from '../../user/services/booking.service'
@Component({
  selector: 'app-view-bookings',
  templateUrl: './view-bookings.component.html',
  styleUrls: ['./view-bookings.component.css']
})
export class ViewBookingsComponent implements OnInit {

  bookings;

  constructor(private bookingService:BookingService) { }

  ngOnInit() {
    this.bookings = this.bookingService.findALlBookings();
    this.bookingService.findALlBookings().subscribe((a:any[]) => {
      console.log(a);
    });
  }
  onCancel(booking) {
    this.bookingService.cancelBooking(booking).subscribe();
    this.ngOnInit();
  }
}
