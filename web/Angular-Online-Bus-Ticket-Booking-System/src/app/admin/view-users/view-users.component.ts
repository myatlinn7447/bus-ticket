import { Component, OnInit } from '@angular/core';
import {UserService} from '../../user/services/user.service';
@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

  users;
  constructor(private userService:UserService) { }


  ngOnInit() {
    this.userService.findAllUsers().subscribe((a:any ) => {
      console.log(a);
      this.users = a
    });
  }

}
