import { BookingService } from './../user/services/booking.service';
import { Component, OnInit } from '@angular/core';

declare const CanvasJS: any;
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private bookingService: BookingService) { }

  ngOnInit() {
    let datas: any[] = [
      { y: 0, label: 'ELITE' },
      { y: 0, label: 'SHWE MANTHU' },
      { y: 0, label: 'MA HAR' },
      { y: 0, label: 'SHWE SIN SETKYAR' },
      { y: 0, label: 'SHWE MANDALAR' },
      { y: 0, label: 'LUMBINI' },
      { y: 0, label: 'SHWE LOON PYAN' },
      { y: 0, label: 'JJ EXPRESS' }
    ];
    this.bookingService.findALlBookings()
      .subscribe((response: any[]) => {
        response.forEach(b => {
          datas.forEach(a => {
            if (b.bus.name === a.label) {
              if (b.seats.length === 0) {
                a.y += b.bus.fare;
              } else {
                a.y += b.bus.fare * b.seats.length;
              }
            }
          })
        });
        let chart = new CanvasJS.Chart("chartContainer", {
          animationEnabled: true,
          exportEnabled: true,
          title: {
            text: "Type U R Self"
          },
          data: [{
            type: "column",
            dataPoints: datas
          }]
        });
        chart.render();
        console.log(response);
        var chart1 = new CanvasJS.Chart("chartContainer1",
          {
            title: {
              text: "Type U R SELF"
            }, axisX: {
              valueFormatString: "MMM YYYY"
            },
            legend: {
              cursor: "pointer",
              verticalAlign: "top",
              horizontalAlign: "center",
              dockInsidePlotArea: true,
            }, toolTip: {
              shared: true
            },
            data: [
              {
                type: "line",
                axisYType: "secondary",
                showInLegend: true,
                markerSize: 0,
                name: 'ELITE',
                dataPoints: response.filter(b => b.bus.name === 'ELITE').map(a => { return { x: new Date(a.key), y: a.seats.length === 0 ? a.bus.fare : a.bus.fare * a.seats.length } })
              },
              {
                type: "line",
                name: 'SHWE MANTHU',
                dataPoints: response.filter(b => b.bus.name === 'SHWE MANTHU').map(a => { return { x: new Date(a.key), y: a.seats.length === 0 ? a.bus.fare : a.bus.fare * a.seats.length } })
              },
              {
                type: "line",
                axisYType: "secondary",
                showInLegend: true,
                markerSize: 0,
                name: 'MA HAR',
                dataPoints: response.filter(b => b.bus.name === 'MA HAR').map(a => { return { x: new Date(a.key), y: a.seats.length === 0 ? a.bus.fare : a.bus.fare * a.seats.length } })
              },
              {
                type: "line",
                name: 'SHWE SIN SETKYAR',
                dataPoints: response.filter(b => b.bus.name === 'SHWE SIN SETKYAR').map(a => { return { x: new Date(a.key), y: a.seats.length === 0 ? a.bus.fare : a.bus.fare * a.seats.length } })
              },
              {
                type: "line",
                name: 'LUMBINI',
                dataPoints: response.filter(b => b.bus.name === 'LUMBINI').map(a => { return { x: new Date(a.key), y: a.seats.length === 0 ? a.bus.fare : a.bus.fare * a.seats.length } })
              },
              {
                type: "line",
                name: 'SHWE LOON PYAN',
                dataPoints: response.filter(b => b.bus.name === 'SHWE LOON PYAN').map(a => { return { x: new Date(a.key), y: a.seats.length === 0 ? a.bus.fare : a.bus.fare * a.seats.length } })
              },
              {
                type: "line",
                name: 'JJ EXPRESS',
                dataPoints: response.filter(b => b.bus.name === 'JJ EXPRESS').map(a => { return { x: new Date(a.key), y: a.seats.length === 0 ? a.bus.fare : a.bus.fare * a.seats.length } })
              },
            ]
          });
        chart1.render();
      });
  }

}